<?php

namespace app\controllers;

class FilmsController extends BaseController
{
    public function viewAction()
    {
        $alias = $this->route['alias'];
        $forms = \R::findAll('orders','status != ?',[1]);
        $contents = \R::find('films',"LIMIT 3");
        $films = \R::findOne('films','alias = ?', [$alias]);
        if (!$films) {
            throw new \Exception('Данный фильм в обработке, обратитесь к администрации за информацией',500);
        }

        $this->setMeta($films->name,'','');
        $this->res(compact('films','contents','forms'));


    }
}