<?php

namespace app\controllers\admin;

use app\models\User;
use kinomonster\base\Controller;
use RedBeanPHP\R;

class BaseController extends Controller
{
    public $layout = 'admin';

    public function __construct($route)
    {
        parent::__construct($route);

        $db = require CONFIG . '/config_db.php';
        class_alias('\R','\RedBeanPHP\R');
        R::setup($db['dsn'],$db['user'],$db['pass']);

        if (!User::authAdmin() && $route['action'] != 'login-admin') {
            header('Location: ' . ADMIN . '/user/login-admin');
        }
    }

    public function getId($get = true, $id = 'id')
    {
        if ($get) {
            $data = $_GET;
        }else {
            $data = $_POST;
        }
        $id = $data[$id] ? $data[$id] : null;
        if (!$id) {
            echo 'fgdfgdgdfg';
        }else {
            return $id;
        }
    }
}