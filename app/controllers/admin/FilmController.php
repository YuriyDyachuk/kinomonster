<?php

namespace app\controllers\admin;

use PDO;
use RedBeanPHP\R;

class FilmController extends BaseController
{
    public function indexAction()
    {
        $films = R::getAll('SELECT films.id,films.title,films.description,films.poster,films.date_pos,films.top,films.publish FROM films GROUP BY films.id');

        $this->setMeta('Список фильмов');
        $this->res(compact('films'));
    }

    public function editAction()
    {
        $id = !empty($_GET['id']) ? $_GET['id'] : null;
        $film = R::findOne('films','id = ?',[$id]);

        $name = !empty($_POST['date_pos']) ? $_POST['date_pos'] : null;
        $publish = !empty($_POST['publish']) ? $_POST['publish'] : null;
        $title = !empty($_POST['title']) ? $_POST['title'] : null;
        $alias = !empty($_POST['alias']) ? $_POST['alias'] : null;
        $top = !empty($_POST['top']) ? $_POST['top'] : null;
        $desc = !empty($_POST['desc']) ? $_POST['desc'] : null;
        $url = !empty($_POST['url']) ? $_POST['url'] : null;
        $upload_image=$_FILES['avatar']['name'];

        $user = 'root';
        $pass = '';

       try {
            $dbcon = new PDO('mysql:host=localhost;dbname=kinomonster', $user, $pass);
            $dbcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

           $myimg = WWW . '/images/' . basename($_FILES['avatar']['name']);

           if (move_uploaded_file($_FILES['avatar']['tmp_name'], $myimg)) {
               $data = $dbcon->prepare("UPDATE films SET alias = {$alias}, title = {$title}, description = {$desc}, poster = {$upload_image}, video = {$url}, date_pos = {$publish} WHERE id = '{$id}' ");

               $data->execute(array(
                 ':id' => $id,
                 ':name' => $name,
                 ':publish' => $publish,
                 ':title' => $title,
                 ':alias' => $alias,
                 ':tops' => $top,
                 ':poster' => $upload_image,
                 ':url' => $url,
                 ':desc_title' => $desc,
               ));
           }

        } catch(PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
        $dbcon = null;

        $this->setMeta('Редактирование фильма');
        $this->res(compact('film'));
    }

    public function addAction()
    {
        $user = 'root';
        $pass = '';

        try {
            $db = new PDO('mysql:host=localhost;dbname=kinomonster', $user, $pass);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $img = WWW . '/pics/' . basename($_FILES['avatar']['name']);

            if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $img)) {
                $data = $db->query("INSERT INTO films (alias, title, description, poster, video, date_pos, created_at) VALUES ('{$_POST['alias_add']}','{$_POST['title_add']}','{$_POST['desc_add']}','{$_FILES['avatar_add']['name']}','{$_POST['url_add']}','{$_POST['date_pos_add']}','{$_POST['publish_add']}')");

                if ($data == true){
//                    header('Location: http://'.$_SERVER['HTTP_HOST'].'/admin/film');
                }else{
                    echo "Информация не занесена в базу данных";
                }
            }

        } catch(PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
        $db = null;

        $this->setMeta('Добавление фильма');
    }
}