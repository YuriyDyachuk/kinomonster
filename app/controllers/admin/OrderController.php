<?php

namespace app\controllers\admin;

use RedBeanPHP\R;

class OrderController extends BaseController
{
    public function indexAction()
    {
        $orders = R::getAll('SELECT orders.id,orders.name,orders.email,orders.phone,orders.hours,orders.row,orders.place_number,orders.date,films.title FROM orders,films WHERE orders.film_id = films.id GROUP BY orders.id ORDER BY orders.status');

        $this->setMeta('Список записей сеансов');
        $this->res(compact('orders'));
    }
}