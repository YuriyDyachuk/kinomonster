<?php

namespace app\controllers\admin;

use app\models\User;
use RedBeanPHP\R;

class UserController extends BaseController
{
    public function indexAction()
    {
        $users = R::findAll('users');

        $this->setMeta('Список пользователей');
        $this->res(compact('users'));
    }


    public function loginAdminAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            if ($user->login(true)) {
                echo 'Ok';
            }else{
                echo 'Логин и пароль не подтвержденны';
            }
            if (User::authAdmin()) {
                header('Location: ' . ADMIN);
            }else{
                header('Location: ');
            }
        }

        $this->layout = 'login-admin';

        $this->setMeta('Вход в админ панель');
    }
}