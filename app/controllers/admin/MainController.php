<?php

namespace app\controllers\admin;

use RedBeanPHP\R;

class MainController extends BaseController
{
    public function indexAction()
    {
        $count_order = R::count('orders',"status = '1'");
        $count_day = R::count('orders','date = ?',[date('Y-m-d')]);
        $count_user = R::count('users');
        $count_film = R::count('films');

        $this->setMeta('Admin panel');
        $this->res(compact('count_order','count_day','count_user','count_film'));
    }

}