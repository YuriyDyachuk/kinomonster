<?php

namespace app\controllers;

use app\models\User;

class UserController extends BaseController
{
    const URL = PATH;
    public function loginAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            $user->login();
            header('Location: ' . self::URL);
        }
        $this->setMeta('Вход');
    }

    public function registerAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            $data = $_POST;
            $user->load($data);
            if (!$user || !$user->emailUnique()) {

            }else{
                $user->attr['password'] = password_hash($user->attr['password'],PASSWORD_DEFAULT);
                if ($user->save('users')) {
                    echo '(())';
                }else{
                    echo 'Что-то пошло не так';
                }
            }
            header('Location: ' . self::URL);
        }
        $this->setMeta('Регистрация','','');
    }

    public function logoutAction()
    {
        if (isset($_SESSION['user'])) unset($_SESSION['user']);
            header('Location: ' . self::URL);
    }
}