<?php

namespace app\controllers;

class MainController extends BaseController
{
    public function indexAction()
    {
        $films = \R::find('films',"top = '1'");
        $contents = \R::find('films',"top = '0' LIMIT 10");

        $this->setMeta('Главная страница','Фильмы для всех','');
        $this->res(compact('films','contents'));

    }

}