<!-- breadcrumb -->
<div class="w3_breadcrumb">
    <div class="breadcrumb-inner">
        <ul>
            <li><a href="<?=PATH?>">Home</a><i>//</i></li>

            <li><?=$films->title ?></li>
        </ul>
    </div>
</div>
<!-- //breadcrumb -->
<!--/content-inner-section-->
<div class="w3_content_agilleinfo_inner">
    <div class="agile_featured_movies">
        <div class="inner-agile-w3l-part-head">
            <div class="w3ls_head_para">
                <h4>Смотреть трейлер к фильму </h4> <p style="font-size: 28px;color: #0a7362"><?=$films->title ?></p>
            </div>
        </div>
        <div class="latest-news-agile-info">
            <div class="col-md-8 latest-news-agile-left-content">
                <div class="single video_agile_player">

                    <div class="video-grid-single-page-agileits">
                        <iframe width="560" height="315" src="<?=$films->video ?>" frameborder="0"
                                allow="accelerometer;
                        autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <p><?=$films->description ?></p>
                </div>
                <br>
                <div class="single-agile-shar-buttons">
                    <h5>Доступные сеансы на <span style="color: #0a7362"><?php echo date('d-m-Y') ?></span></h5>
                    <div>
                        <button id="toggleLink" href="javascript:void(0);" onclick="viewdiv('mydiv');"
                                data-text-hide="Открыть блок" data-text-show="Скрыть блок"
                                class="btn btn-primary" style="border-radius: 5%">Открыть блок</button>
                    </div>
                </div>
                <div class="all-comments-info" id="mydiv" style="display: none">
                    <h5 >Свободные места</h5>
                    <div class="agile-info-wthree-box">
                        <form action="form.php" method="POST">
                            <div class="table-responsive-sm">
                                <?php $data = [1, 2, 3, 4, 5]; ?>
                                <?php $data_col = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; ?>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">Ряд</th>
                                            <th scope="col" colspan="10" style="text-align: center;">Место</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row" class="item-row"><input type="checkbox" name="row"
                                                                                  value="1" class="row">1</th>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="1">1</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="2">2</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="3">3</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="4">4</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="5">5</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="6">6</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="7">7</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="8">8</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="9">9</td>
                                            <td class="item-cnt"><input type="checkbox" name="col" value="10">10</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="th-1" data-toggle="2">2</th>
                                            <td class="td-1"> 1</td>
                                            <td class="td-2"> 2</td>
                                            <td class="td-3"> 3</td>
                                            <td class="td-4"> 4</td>
                                            <td class="td-5"> 5</td>
                                            <td class="td-6"> 6</td>
                                            <td class="td-7"> 7</td>
                                            <td class="td-8"> 8</td>
                                            <td class="td-9"> 9</td>
                                            <td class="td-10"> 10</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="th-1" data-toggle="3">3</th>
                                            <td class="td-1">1</td>
                                            <td class="td-2">2</td>
                                            <td class="td-3">3</td>
                                            <td class="td-4">4</td>
                                            <td class="td-5">5</td>
                                            <td class="td-6">6</td>
                                            <td class="td-7">7</td>
                                            <td class="td-8">8</td>
                                            <td class="td-9">9</td>
                                            <td class="td-10">10</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="th-1" data-toggle="4">4</th>
                                            <td class="td-1">1</td>
                                            <td class="td-2">2</td>
                                            <td class="td-3">3</td>
                                            <td class="td-4">4</td>
                                            <td class="td-5">5</td>
                                            <td class="td-6">6</td>
                                            <td class="td-7">7</td>
                                            <td class="td-8">8</td>
                                            <td class="td-9">9</td>
                                            <td class="td-10">10</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="th-1" data-toggle="5">5</th>
                                            <td class="td-1">1</td>
                                            <td class="td-2">2</td>
                                            <td class="td-3">3</td>
                                            <td class="td-4">4</td>
                                            <td class="td-5">5</td>
                                            <td class="td-6">6</td>
                                            <td class="td-7">7</td>
                                            <td class="td-8">8</td>
                                            <td class="td-9">9</td>
                                            <td class="td-10">10</td>
                                        </tr>
                                        </tbody>
                                    </table>
                            </div>
                            &nbsp;
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Выбрать время :</label>
                                <select class="form-control" name="hours" id="hours" >
                                    <option>---</option>
                                    <option value="10:00">10:00</option>
                                    <option value="12:00">12:00</option>
                                    <option value="14:00">14:00</option>
                                    <option value="16:00">16:00</option>
                                    <option value="18:00">18:00</option>
                                    <option value="20:00">20:00</option>
                                </select>
                            </div>
                            <div class="col-md-6 form-info">
                                <input type="text" name="name" class="name" placeholder="Ваше имя" required="">
                                <input type="email" name="email" class="email" placeholder="Ваш E-mail" required="">
                                <input type="text" name="phone" class="phone" placeholder="Ваш номер телефона"
                                       required="">
                                &nbsp;<input type="hidden" name="id" class="id" value="<?=$films->id?>">
                                &nbsp;<input type="hidden" name="alias" class="alias" value="<?=$films->alias?>">
                                <input type="submit" value="Запись на сеанс" class="button">
                            </div>
                          <div class="clearfix"> </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 latest-news-agile-right-content">
                    <h4 class="side-t-w3l-agile">Информация о <span>фильме</span></h4>
                        <div class="video_agile_player sidebar-player">
                            <div class="hvr-sweep-to-bottom"">
                                <img src="images/<?=$films->poster?>" alt=""
                                class="img-responsive">
                            </div>
                            <div class="player-text side-bar-info">
                                <p class="fexi_header"><?=$films->title ?> </p>
                                <p class="fexi_header_para">Описание : </p>
                                <p class="fexi_header_para"><span
                                      class="conjuring_w3"></span><?=mb_strimwidth
                                    ($films->description,0,'500','...')
                                    ?></p>
                                <p class="fexi_header_para"><span>В кино
                                        с<label>:</label></span><?=$films->date_pos?></p>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
</div>
<!--//content-inner-section-->