<div class="w3_content_agilleinfo_inner">
    <div class="agile_featured_movies">
        <!--//tab-section-->
        <h3 class="agile_w3_title"> TOP <span>просмотров</span></h3>
        <!--/movies-->
        <div class="w3_agile_latest_movies">

            <div id="owl-demo" class="owl-carousel owl-theme">
                <?php foreach ($films as $film): ?>
                <div class="item">
                    <div class="w3l-movie-gride-agile w3l-movie-gride-slider ">
                        <a href="films/<?=$film->alias?>" class="hvr-sweep-to-bottom"><img
                              src="images/<?=$film->poster?>" title="<?=$film->title?>" class="img-responsive" alt="
" />
                            <div class="w3l-action-icon"><i class="fa fa-play-circle-o" aria-hidden="true"></i></div>
                        </a>
                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="films/<?=$film->alias?>" title="<?=$film->title?>"><?=mb_strimwidth
                                        ($film->title,0,18,'...')
                                        ?></a></h6>
                            </div>
                        </div>
                        <div class="ribben one">
                            <p>TOP</p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <!--//movies-->
        <h3 class="agile_w3_title" style="text-align: center">Все <span>фильмы</span> </h3>
        <!--/requested-movies-->
        <div class="wthree_agile-requested-movies">
            <?php foreach ($contents as $content): ?>
            <div class="col-md-2 w3l-movie-gride-agile requested-movies">
                <a href="films/<?=$content->alias?>" class="hvr-sweep-to-bottom"><img
                      src="images/<?=$content->poster?>" title="<?=$content->title?>"
                                                                       class="img-responsive" alt=" ">
                    <div class="w3l-action-icon"><i class="fa fa-play-circle-o" aria-hidden="true"></i></div>
                </a>
                <div class="mid-1 content agileits_w3layouts_mid_1_home" style="height: 200px !important;">
                    <div class="w3l-movie-text">
                        <h6><a href="films/<?=$content->alias?>" title="<?=$content->title?>"><?=mb_strimwidth($content->title,0,15,'...')
                                ?></a></h6>
                        &nbsp;
                        <p><a><?=mb_strimwidth($content->description,0,100,'...') ?></a></p>
                    </div>
                    <div class="mid-2 agile_mid_2_home">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="clearfix"></div>
        </div>
        <!--//requested-movies-->
    </div>
</div>
<!--//content-inner-section-->