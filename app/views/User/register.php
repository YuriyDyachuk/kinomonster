<?php /*\Composer\Autoload\includeFile('valid_form.php')*/?>
<div class="container-fluid">
    <h2>Регистрация</h2>
    <form class="form-horizontal" action="user/register" method="post" data-toggle="validator">
        <div class="form-group">
            <label class="control-label col-xs-3" for="firstName">Имя:</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="name" id="firstName" placeholder="Введите имя">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="inputEmail">Email:</label>
            <div class="col-md-6">
                <input type="email" class="form-control" name="email" id="inputEmail" value="<?=$_POST['email'] ?>"
                placeholder="Введите Email">
                <span class="has-error"><?php  ?></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="phoneNumber">Телефон:</label>
            <div class="col-md-6">
                <input type="tel" class="form-control" name="phone" id="phoneNumber" placeholder="Введите номер телефона">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="postalAddress">Логин:</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="login" id="firstLogin" placeholder="Введите логин">
                <span class="has-error"><?php  ?></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="inputPassword">Пароль:</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="password" id="inputPassword"
                       placeholder="Введите пароль">
            </div>
        </div>
        <br />
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-9">
                <input type="submit" class="btn btn-primary" value="Регистрация">
                <input type="reset" class="btn btn-default" value="Очистить форму">
            </div>
        </div>
    </form>
</div>