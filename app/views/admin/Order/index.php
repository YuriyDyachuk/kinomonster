<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">
            <li class=""><a href="<?= ADMIN ?>">Главная</a></li>
            <li class="active">Список записей на сеанс</li>
        </ol>
        <div class="page-heading">
        </div>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" data-widget=''>
                            <div class="panel-heading">
                                <h2>Todo List</h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box">
                                                <div class="box-body">
                                                    <table class="table table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">№</th>
                                                            <th scope="col">Имя</th>
                                                            <th scope="col">E-mail</th>
                                                            <th scope="col">Номер телефона</th>
                                                            <th scope="col">Дата сеанса</th>
                                                            <th scope="col">Время сеанса</th>
                                                            <th scope="col">Название фильма</th>
                                                            <th scope="col">Ряд</th>
                                                            <th scope="col">Место</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach($orders as $order): ?>
                                                        <tr>
                                                            <th scope="row"><?=$order['id']?></th>
                                                            <td><?=$order['name']?></td>
                                                            <td><?=$order['email']?></td>
                                                            <td><?=$order['phone']?></td>
                                                            <td><?=$order['date']?></td>
                                                            <td><?=$order['hours']?></td>
                                                            <td><?=$order['title']?></td>
                                                            <td><?=$order['row']?></td>
                                                            <td><?=$order['place_number']?></td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="panel-editbox" data-widget-controls=""></div>
                                <div class="todo-footer clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- .container-fluid -->
    </div> <!-- #page-content -->
</div>
