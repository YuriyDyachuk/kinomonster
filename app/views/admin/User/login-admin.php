<div class="container" id="login-form" style="margin-top: 20%">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="panel panel-default">
        <h4 style="text-align: center">Login Form</h4>
        <div class="panel-body">

          <form action="<?= ADMIN ?>/user/login-admin" method="post" class="form-horizontal" id="validate-form">
            <div class="form-group">
              <div class="col-xs-12">
                <div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-user"></i>
									</span>
                  <input type="text" class="form-control" name="login" placeholder="Login Username"
                         data-parsley-minlength="6"
                         placeholder="At least 6 characters" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-xs-12">
                <div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-key"></i>
									</span>
                  <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                         placeholder="Password">
                </div>
              </div>
            </div>

            <div class="panel-footer">
              <div class="clearfix">
                <button type="submit" class="btn btn-primary pull-right">Login</button>
              </div>
            </div>

          </form>
        </div>
      </div>

    </div>
  </div>
</div>