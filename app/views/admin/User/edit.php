<div class="static-content">
  <div class="page-content">
    <ol class="breadcrumb">
      <li class=""><a href="<?= ADMIN ?>">Главная</a></li>
      <li class=""><a href="<?= ADMIN ?>/user">Список пользователей</a></li>
      <li class="active">Редактирование пользователя</li>
    </ol>
    <div class="page-heading">
    </div>
    <div class="container-fluid">
      <div data-widget-group="group1">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default" data-widget=''>
              <div class="panel-heading">
                <h2>Todo List</h2>
                <div class="row">
                  <div class="col-md-12">
                    <div class="box">
                      <form action="update.php" method="post" data-toggle="validator">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="login">Логин</label>
                            <input type="text" class="form-control" name="login"
                                   id="login" value="<?= htmlspecialchars($user->login) ?>" required>
                          </div>
                          <div class="form-group">
                            <label for="password">Пароль</label>
                            <input type="text" class="form-control" name="password"
                                   id="password" placeholder="Введите пароль">
                          </div>
                          <div class="form-group">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" name="nane"
                                   id="name" value="<?= htmlspecialchars($user->name) ?>"
                                   required>
                          </div>
                          <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" name="email"
                                   id="email" value="<?= htmlspecialchars($user->email) ?>"
                                   required>
                          </div>
                          <div class="form-group">
                            <label>Роль</label>
                            <select name="role" id="role" class="form-control">
                              <option value="user"
                                <?php if ($user->role == 'user') {
                                    echo 'selected';
                               }
                                ?>>Пользоваль
                              </option>
                              <option value="admin"
                                <?php if ($user->role == 'admin') {
                                    echo 'selected';
                                }
                                ?>>Админ
                              </option>
                            </select>
                          </div>
                        </div>
                        <div class="box-body">
                          <input type="hidden" name="id" value="<?=$user->id?>">
                          <button type="submit" class="btn btn-primary">Подтвердить</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div class="panel-editbox" data-widget-controls=""></div>
              <div class="todo-footer clearfix"></div>
            </div>
          </div>
        </div>
      </div>

    </div> <!-- .container-fluid -->
  </div> <!-- #page-content -->
</div>
