<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">
            <li class=""><a href="<?= ADMIN ?>">Главная</a></li>
            <li class="active">Список фильмов</li>
        </ol>
        <div class="page-heading">
        </div>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" data-widget=''>
                            <div class="panel-heading">
                              <h2>Просмотр фильмов</h2>
                              <button class="btn btn-success pull-right" id="button-add" style="margin-top: 5px">
                                  <a href="admin/film/add" style="color: white">Добавить новый фильм</a>
                              </button>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-body">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">№</th>
                                                        <th scope="col">Название фильма</th>
                                                        <th scope="col">Аватар</th>
                                                        <th scope="col">Дата выхода</th>
                                                        <th scope="col">Опубликованые</th>
                                                        <th scope="col">TOP</th>
                                                        <th scope="col">Редактирование</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach($films as $film): ?>
                                                        <tr>
                                                            <th scope="row"><?=$film['id']?></th>
                                                            <td><?=$film['title']?></td>
                                                            <td><img src="images/<?=$film['poster']?>" style="width:
                                                            100px"
                                                                     class="img-responsive" alt=" "></td>
                                                            <td><?=$film['date_pos']?></td>
                                                            <?php if ($film['publish'] == 0 ): ?>
                                                                <td>Опубликовано</td>
                                                            <?php else: ?>
                                                                <td>Публикации нет</td>
                                                            <?php endif; ?>
                                                            <?php if ($film['top'] == 1 ): ?>
                                                            <td>В топе</td>
                                                            <?php else: ?>
                                                            <td>Не в топе</td>
                                                            <?php endif; ?>
                                                            <td><a href="<?= ADMIN ?>/film/edit?id=<?=$film['id']?>"><i
                                                                  class="fa
                                                            fa-eye-slash" style="margin-left: 50px"></i></a></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-editbox" data-widget-controls=""></div>
                            <div class="todo-footer clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- .container-fluid -->
    </div> <!-- #page-content -->
</div>
