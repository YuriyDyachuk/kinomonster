<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">
            <li class=""><a href="<?= ADMIN ?>">Главная</a></li>
            <li class=""><a href="<?= ADMIN ?>/film">Список фильмов</a></li>
            <li class="active">Редактирование фильма</li>
        </ol>
        <div class="page-heading">
        </div>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" data-widget=''>
                            <div class="panel-heading">
                                <h2>Форма для редактирования</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <form action="" method="post" data-toggle="validator"
                                                  enctype="multipart/form-data">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="alias">Алиас фильма:</label>
                                                        <input type="text" class="form-control" name="alias"
                                                               id="alias" value="<?=$film->alias?>"
                                                        >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="title">Название фильма:</label>
                                                        <input type="text" class="form-control" name="title"
                                                               id="title" value="<?=$film->title?>"
                                                               >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Дата выхода фильма:</label>
                                                        <input type="date" class="form-control" name="date_pos"
                                                               id="date_pos" value="<?=$film->date_pos?>"
                                                               >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Дата публикации фильма:</label>
                                                        <input type="date" class="form-control" name="publish"
                                                               id="publish" value="<?=$film->created_at?>"
                                                        >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Изменить рейтинг фильма:</label>
                                                        <select class="form-control" name="top" id="top">
                                                            <option value="0" name="top-one">В
                                                                топе</option>
                                                            <option value="1" name="top-two">Не в топе</option>
                                                        </select>
                                                    </div>
                                                    &nbsp;
                                                    <label>Описание к фильму:</label>
                                                    <div class="form-group">
                                                        <textarea name="desc" id="desc" cols="120"
                                                        rows="5"><?=$film->description?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Сылка на трейлер:</label>
                                                        <input type="text" class="form-control" name="url"
                                                               id="url" value="<?=$film->video?>"
                                                        >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Изображение фильма:</label>
                                                        <img src="images/<?=$film->poster?>" style="width:
                                                            150px;float: right"
                                                             class="img-responsive" alt=" ">
                                                        <input type="file" class="form-control" name="avatar"
                                                               id="avatar">
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <input type="hidden" name="id" value="<?=$data->id?>">
                                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-editbox" data-widget-controls=""></div>
                            <div class="todo-footer clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- .container-fluid -->
    </div> <!-- #page-content -->
</div>
