<?php

//
$name_error = $email_error = $phone_error = $pass_error = $login_error = '';
$name = $email = $phone = $pass = $login = '';

//
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['name'])) {
        $name_error = 'Веддите имя';
    }else{
        $name = test_input($_POST['name']);
        //
        if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
            $name_error ='Только символы букв';
        }
    }

    //
    if (empty($_POST['email'])) {
        $email_error = 'Веддите email';
    }else{
        $email = test_input($_POST['email']);
        //
        if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
            $email_error ='Веддите коректный E-mail';
        }
    }

    //
   /* if (empty($_POST['phone'])) {
        $phone_error = 'Веддите number';
    }else{
        $phone = test_input($_POST['phone']);
        //
        if (!preg_match("",$phone)) {
            $phone_error ='Веддите коректный номер';
        }
    }*/

    //
    /*if (empty($_POST['login'])) {
        $login_error = 'Веддите имя';
    }else{
        $login = test_input($_POST['login']);
        //
        if (!preg_match("/^[a-z0-9-A-Z ]*$/", $login)) {
            $name_error ='Только символы букв';
        }
    }*/

    //
    /*if (empty($_POST['password'])) {
        $pass_error = 'Веддите имя';
    }else{
        $pass = test_input($_POST['password']);
        //
        if (!preg_match("/^[a-z0-9-A-Z ]*$/", $pass)) {
            $pass_error ='Только символы букв';
        }
    }*/

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


}