<?php

namespace kinomonster\base;

use kinomonster\Database;
use RedBeanPHP\R;

abstract class Model
{
    public $attr = [];
    public $errors = [];
    public $rules = [];

    public function __construct()
    {
        Database::instance();
    }

    public function load($data) {
        foreach ($this->attr as $name=>$val) {
            if (isset($data[$name])) {
                $this->attr[$name] = $data[$name];
            }
        }
    }

    public function save($table)
    {
        $res = \R::dispense($table);
        foreach ($this->attr as $name => $value) {
            $res->$name = $value;
        }
        return \R::store($res);
    }

    public function update($table, $id)
    {
        $bean = \R::load(table,$id);
        foreach ($this->attr as $name => $value) {
            $bean->$name = $value;
        }
        return \R::store($bean);
    }
}