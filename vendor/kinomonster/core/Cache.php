<?php

namespace kinomonster;

class Cache
{
    use Base;

    public function setter($val, $data, $time = 3600)
    {
        if ($time) {
            $res['data'] = $data;
            $res['end_time'] = time() + $time;
            if (file_put_contents(CACHE . '/' . $val . 'txt', serialize($res))) {
                return true;
            }
        }
        return false;
    }

    public function getter($val)
    {
        $file = CACHE . '/' . $val . 'txt';
        if (file_exists($file)) {
            $res = unserialize(file_get_contents($file));
            if (time() <= $res['end_time']){
                return $res;
            }
            unlink($file);
        }
        return false;
    }

    public function delete($val)
    {
        $file = CACHE . '/' . $val . 'txt';
        if (file_exists($file)) {
            unlink($file);
        }
    }
}